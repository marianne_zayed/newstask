package com.example.news.models;

/**
 * Created by Marianne.Wazif on 29-Dec-16.
 */

public class UserLogin {
    private String fullName;
    private String email;
    private int socialMedia;

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getSocialMedia() {
        return socialMedia;
    }

    public void setSocialMedia(int socialMedia) {
        this.socialMedia = socialMedia;
    }
}

package com.example.news.adapters;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.example.news.helpers.GlobalState;
import com.example.news.helpers.Services;
import com.example.news.helpers.Utility;
import com.example.news.models.News;
import com.example.news.R;
import com.example.news.fragments.FragmentListingNews.OnListFragmentInteractionListener;
import com.example.news.views.CircularNetworkImageView;
// import com.example.news.dummy.DummyContent.News;

import java.util.ArrayList;
import java.util.Locale;

/**
 * {@link RecyclerView.Adapter} that can display a {@link News} and makes a call to the
 * specified {@link OnListFragmentInteractionListener}.
 * TODO: Replace the implementation with code for your data type.
 */
public class MyNewsRecyclerViewAdapter extends RecyclerView.Adapter<MyNewsRecyclerViewAdapter.ViewHolder> {

    private final ArrayList<News> mValues;
    private final OnListFragmentInteractionListener mListener;
    private Context mContext;
    private ImageLoader mImageLoader;
    private GlobalState mApplication;

    public MyNewsRecyclerViewAdapter(ArrayList<News> items, OnListFragmentInteractionListener listener, Context context) {
        mValues = items;
        mListener = listener;
        mContext = context;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_list_item, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        mApplication = (GlobalState) mContext.getApplicationContext();
        holder.mNews = mValues.get(position);
        mImageLoader = Services.getInstance(mContext).getImageLoader();

        holder.mIcon.setDefaultImageResId(R.drawable.filter); //Placeholder
        holder.mIcon.setImageUrl(holder.mNews.getImageUrl(), mImageLoader);
        holder.mTitle.setText(holder.mNews.getNewsTitle());
        holder.mNewsType.setImageResource(Utility.getImageResourceForNewsType(holder.mNews.getNewsType()));
        Locale locale = new Locale(mApplication.getLocale());
        holder.mDate.setText(Utility.formatDate("dd MMM, yyyy", holder.mNews.getPostDate(), locale));
        holder.mLikes.setText(mContext.getResources().getString(R.string.likes) + "(" + holder.mNews.getLikes() + ")");
        holder.mViews.setText(holder.mNews.getNumofViews() + " " + mContext.getResources().getString(R.string.views));
        holder.mCardView.setCardBackgroundColor(ContextCompat.getColor(mContext, R.color.transparentWhite));

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.

                    mListener.onListFragmentInteraction(holder.mNews);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final CircularNetworkImageView mIcon;
        public final TextView mTitle;
        public final ImageView mNewsType;
        public final TextView mDate;
        public final TextView mLikes;
        public final TextView mViews;
        public News mNews;
        public CardView mCardView;


        public ViewHolder(View view) {
            super(view);
            mView = view;
            mIcon = (CircularNetworkImageView) view.findViewById(R.id.imgItemNewsIcon);
            mTitle = (TextView) view.findViewById(R.id.txtItemNewsTitle);
            mNewsType = (ImageView) view.findViewById(R.id.imgItemNewsType);
            mDate = (TextView) view.findViewById(R.id.txtItemNewsDate);
            mLikes = (TextView) view.findViewById(R.id.txtItemNewsLikes);
            mViews = (TextView) view.findViewById(R.id.txtItemNewsViews);
            mCardView = (CardView) view.findViewById(R.id.crdNewsItem);
        }


    }

}

package com.example.news.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.news.R;
import com.example.news.helpers.Utility;
import com.example.news.models.UserLogin;

import java.util.ArrayList;

/**
 * Created by Marianne on 12-Nov-16.
 */

public class DrawerItemsAdapter extends RecyclerView.Adapter<DrawerItemsAdapter.ViewHolder> {
    private static final int TYPE_HEADER = 0;
    private static final int TYPE_ITEM = 1;
    private ArrayList<String> mDrawerItems = new ArrayList<>();
    private DrawerItemClickListener mDrawerItemClickListener;
    private UserLogin mUserLogin;

    public DrawerItemsAdapter(ArrayList<String> drawerItems, UserLogin userLogin, DrawerItemClickListener drawerItemClickListener) {
        mDrawerItems = drawerItems;
        mUserLogin = userLogin;
        mDrawerItemClickListener = drawerItemClickListener;
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        private int holderId;
        private ImageView iconView;
        private TextView titleView;
        private TextView name;
        private TextView email;
        private final View mView;

        ViewHolder(View view, int viewType) {
            super(view);
            mView = view;
            if (viewType == TYPE_ITEM) {
                iconView = (ImageView) view.findViewById(R.id.imgItemNewsIcon);
                titleView = (TextView) view.findViewById(R.id.list_item_title_textview);
                holderId = TYPE_ITEM;
            } else if (viewType == TYPE_HEADER) {
                name = (TextView) view.findViewById(R.id.txtUserName);
                email = (TextView) view.findViewById(R.id.txtEmail);
                holderId = TYPE_HEADER;
            }
        }

    }


    @Override
    public void onBindViewHolder(final DrawerItemsAdapter.ViewHolder holder, int position) {
        if (holder.holderId == TYPE_ITEM) {
            holder.iconView.setImageResource(Utility.getImageResourceForDrawerItems(position - 1));
            holder.titleView.setText(mDrawerItems.get(position - 1));
        } else {
            holder.name.setText(mUserLogin.getFullName());
            holder.email.setText(mUserLogin.getEmail());
        }
        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mDrawerItemClickListener) {

                    mDrawerItemClickListener.onClick(v, holder.getAdapterPosition());
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mDrawerItems.size() + 1;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ViewHolder viewHolder;
        if (viewType == TYPE_ITEM) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.drawer_list_item, parent, false);
            viewHolder = new ViewHolder(view, viewType);
            return viewHolder;
        } else if (viewType == TYPE_HEADER) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.nav_header_main, parent, false); //Inflating the layout
            viewHolder = new ViewHolder(v, viewType); //Creating ViewHolder and passing the object of type view
            return viewHolder; //returning the object created


        }
        return null;
    }

    @Override
    public int getItemViewType(int position) {
        if (isPositionHeader(position))
            return TYPE_HEADER;

        return TYPE_ITEM;
    }

    private boolean isPositionHeader(int position) {
        return position == TYPE_HEADER;
    }

    public interface DrawerItemClickListener {
        void onClick(View view, int position);
    }

}

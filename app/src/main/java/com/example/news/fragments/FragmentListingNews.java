package com.example.news.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.example.news.helpers.Constants;
import com.example.news.helpers.ParseNewsResponse;
import com.example.news.adapters.MyNewsRecyclerViewAdapter;
import com.example.news.helpers.Utility;
import com.example.news.helpers.Services;
import com.example.news.models.News;
import com.example.news.R;

import org.json.JSONObject;

import java.util.ArrayList;

import static com.example.news.helpers.Constants.SELECTED_KEY;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Activities containing this fragment MUST implement the {@link OnListFragmentInteractionListener}
 * interface.
 */
public class FragmentListingNews extends Fragment {


    private OnListFragmentInteractionListener mListener;
    private ArrayList<News> mNewsArrayList = new ArrayList<>();
    private MyNewsRecyclerViewAdapter myNewsRecyclerViewAdapter;
    private ProgressBar mProgressBar;
    private boolean mTwoPane;
    private int mPosition = RecyclerView.NO_POSITION;
    private RecyclerView mRcyNewsList;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public FragmentListingNews() {
    }

    public static FragmentListingNews newInstance(boolean twoPane) {
        Bundle args = new Bundle();
        args.putBoolean(Constants.LISTING_FRAGMENT_PARAM, twoPane);
        FragmentListingNews fragmentListingNews = new FragmentListingNews();
        fragmentListingNews.setArguments(args);
        return fragmentListingNews;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Bundle arguments = getArguments();
        if (arguments != null) {
            mTwoPane = arguments.getBoolean(Constants.LISTING_FRAGMENT_PARAM);
        }
        View view = inflater.inflate(R.layout.fragment_list, container, false);


        mRcyNewsList = (RecyclerView) view.findViewById(R.id.rcyNewsList);
        mProgressBar = (ProgressBar) view.findViewById(R.id.progressBar);
        mProgressBar.setVisibility(View.VISIBLE);
        mRcyNewsList.setLayoutManager(new LinearLayoutManager(getActivity()));
        myNewsRecyclerViewAdapter = new MyNewsRecyclerViewAdapter(mNewsArrayList, mListener, getActivity());

        mRcyNewsList.setAdapter(myNewsRecyclerViewAdapter);
        if (savedInstanceState != null && savedInstanceState.containsKey(SELECTED_KEY)) {

            mPosition = savedInstanceState.getInt(SELECTED_KEY);
        }
        mRcyNewsList.getLayoutManager().scrollToPosition(mPosition);
        return view;
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {

        super.onActivityCreated(savedInstanceState);
        if (Utility.haveNetworkConnection(getActivity())) {
            getData();
        } else {
            Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.error_msg), Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (mPosition != RecyclerView.NO_POSITION) {
            outState.putInt(SELECTED_KEY, mPosition);
        }
    }

    private void getData() {
        Services.getInstance(getActivity()).getNews(new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                mNewsArrayList.addAll((ParseNewsResponse.getInstance(getActivity()).getNewsDataFromJson(response)).getNews());
                myNewsRecyclerViewAdapter.notifyDataSetChanged();
                mProgressBar.setVisibility(View.GONE);
                if (mTwoPane)
                    mListener.onListFragmentInteraction(mNewsArrayList.get(0));


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.error_msg), Toast.LENGTH_LONG).show();
            }
        });


    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        if (context instanceof OnListFragmentInteractionListener) {
            mListener = (OnListFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnListFragmentInteractionListener");
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (Services.getInstance(getActivity()).getRequestQueue() != null) {
            Services.getInstance(getActivity()).getRequestQueue().cancelAll(Services.Tag.NEWS);
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnListFragmentInteractionListener {
        void onListFragmentInteraction(News item);
    }

}

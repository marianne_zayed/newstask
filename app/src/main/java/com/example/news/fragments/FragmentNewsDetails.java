package com.example.news.fragments;

import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.ShareActionProvider;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.example.news.helpers.Constants;
import com.example.news.helpers.GlobalState;
import com.example.news.helpers.ParseNewsResponse;
import com.example.news.helpers.Utility;
import com.example.news.models.NewsDetailsResponse;
import com.example.news.helpers.Services;
import com.example.news.R;

import org.json.JSONObject;

import java.util.Locale;

/**
 * A placeholder fragment containing a simple view.
 */
public class FragmentNewsDetails extends Fragment {
    private ShareActionProvider mShareActionProvider;
    private String mNewsID;
    private TextView mNewsTitle;
    private NetworkImageView mNewsImage;
    private TextView mNewsDate;
    private TextView mNewsLikes;
    private TextView mNewsViews;
    private TextView mNewsDescription;
    private ImageLoader mimageLoader;
    private String mShareURL;
    private ProgressBar mProgressBar;
    private ScrollView mScrlNewsDetails;
    private boolean mTwoPane;
    private ImageView mImgShareNews;
    private GlobalState mApplication;

    public static FragmentNewsDetails newInstance(boolean twoPane, String newsID) {

        Bundle args = new Bundle();
        args.putBoolean(Constants.TWO_PANE, twoPane);
        args.putString(Constants.DETAIL_FRAGMENT_PARAM, newsID);
        FragmentNewsDetails fragmentNewsDetails = new FragmentNewsDetails();
        fragmentNewsDetails.setArguments(args);
        return fragmentNewsDetails;
    }

    public FragmentNewsDetails() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        mApplication = (GlobalState) getActivity().getApplicationContext();
        Bundle arguments = getArguments();
        if (arguments != null) {
            mNewsID = arguments.getString(Constants.DETAIL_FRAGMENT_PARAM);
            mTwoPane = arguments.getBoolean(Constants.TWO_PANE);
        }


        View rootView = inflater.inflate(R.layout.fragment_news_details, container, false);
        findViewByID(rootView);
        if (mTwoPane) {
            mImgShareNews.setVisibility(View.VISIBLE);
        } else {
            setHasOptionsMenu(true);
        }

        setListeners();

        return rootView;
    }

    private void findViewByID(View rootView) {
        mProgressBar = (ProgressBar) rootView.findViewById(R.id.progressBar);
        mProgressBar.setVisibility(View.VISIBLE);
        mNewsTitle = (TextView) rootView.findViewById(R.id.txtNewsTitle);
        mNewsImage = (NetworkImageView) rootView.findViewById(R.id.imgNews);
        mNewsDate = (TextView) rootView.findViewById(R.id.txtNewsDate);
        mNewsLikes = (TextView) rootView.findViewById(R.id.txtNewsLikes);
        mNewsViews = (TextView) rootView.findViewById(R.id.txtNewsViews);
        mNewsDescription = (TextView) rootView.findViewById(R.id.txtNewsDescription);
        mScrlNewsDetails = (ScrollView) rootView.findViewById(R.id.scrlNewsDetails);
        mImgShareNews = (ImageView) rootView.findViewById(R.id.imgShareNews);
    }

    private void setListeners() {
        mImgShareNews.setOnClickListener(imgShareNewsClickListener);
    }

    private View.OnClickListener imgShareNewsClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent shareIntent = createShareNewsIntent();
            startActivity(shareIntent);

        }
    };

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (Utility.haveNetworkConnection(getActivity())) {
            if (mNewsID != null) {
                GetNewsDetails();
            }
        } else {
            Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.no_internet_connection), Toast.LENGTH_LONG).show();
        }
    }

    private void GetNewsDetails() {
        Services.getInstance(getActivity()).getNewsDetails(mNewsID, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                getNewsDetailsFromJson(response);

                mProgressBar.setVisibility(View.GONE);
                mScrlNewsDetails.setVisibility(View.VISIBLE);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.error_msg), Toast.LENGTH_LONG).show();
            }
        });

    }

    private void getNewsDetailsFromJson(JSONObject response) {
        NewsDetailsResponse newsDetailsResponse = ParseNewsResponse.getInstance(getActivity()).getNewsDetailsDataFromJson(response);
        mNewsTitle.setText(newsDetailsResponse.getNewsItem().getNewsTitle());
        mimageLoader = Services.getInstance(getActivity()).getImageLoader();
        mNewsImage.setImageUrl(newsDetailsResponse.getNewsItem().getImageUrl(), mimageLoader);
        Locale locale = new Locale(mApplication.getLocale());
        mNewsDate.setText(Utility.formatDate("dd MMM, yyyy", newsDetailsResponse.getNewsItem().getPostDate(), locale));

        mNewsLikes.setText(getActivity().getResources().getString(R.string.likes) + " (" + newsDetailsResponse.getNewsItem().getLikes() + ")");

        mNewsViews.setText(newsDetailsResponse.getNewsItem().getNumofViews() + " " + getActivity().getResources().getString(R.string.views));
        mNewsDescription.setText(newsDetailsResponse.getNewsItem().getItemDescription());
        mShareURL = newsDetailsResponse.getNewsItem().getShareURL();
        if (!mTwoPane)
            mShareActionProvider.setShareIntent(createShareNewsIntent());

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // Inflate the menu; this adds items to the action bar if it is present.
        inflater.inflate(R.menu.menu_news_details, menu);

        // Retrieve the share menu item
        MenuItem menuItem = menu.findItem(R.id.action_share);

        // Get the provider and hold onto it to set/change the share intent.
        mShareActionProvider = (ShareActionProvider) MenuItemCompat.getActionProvider(menuItem);


        mShareActionProvider.setShareIntent(createShareNewsIntent());

    }

    private Intent createShareNewsIntent() {
        Intent shareIntent = new Intent(Intent.ACTION_SEND);
        shareIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_DOCUMENT);
        shareIntent.setType("text/plain");
        shareIntent.putExtra(Intent.EXTRA_TEXT, mShareURL);
        return shareIntent;
    }

    @Override
    public void onStop() {
        super.onStop();
        if (Services.getInstance(getActivity()).getRequestQueue() != null) {
            Services.getInstance(getActivity()).getRequestQueue().cancelAll(Constants.DETAILSTAG);
        }
    }
}

package com.example.news.fragments;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.news.R;
import com.example.news.helpers.Utility;

/**
 * A placeholder fragment containing a simple view.
 */
public class FragmentLogin extends Fragment {
    private Button btnFacebook;
    private Button btnTwitter;
    private Button btnGoogle;
    private OnRegisterCallbackListener mListener;

    public static FragmentLogin newInstance() {
        return new FragmentLogin();
    }

    public FragmentLogin() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Utility.getHashKey(getActivity());
        View rootView = inflater.inflate(R.layout.fragment_login, container, false);
        btnFacebook = (Button) rootView.findViewById(R.id.btnFacebook);
        btnTwitter = (Button) rootView.findViewById(R.id.btnTwitter);
        btnGoogle = (Button) rootView.findViewById(R.id.btnGoogle);
        setListeners();
        return rootView;
    }

    private void setListeners() {
        btnFacebook.setOnClickListener(btnFacebookClick);
        btnGoogle.setOnClickListener(btnGoogleClick);
        btnTwitter.setOnClickListener(btnTwitterClick);
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        if (context instanceof OnRegisterCallbackListener) {
            mListener = (OnRegisterCallbackListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnRegisterCallbackListener");
        }
    }

    private View.OnClickListener btnFacebookClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (null != mListener) {
                mListener.loginFacebook();
            }
        }
    };

    private View.OnClickListener btnGoogleClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (null != mListener) {
                mListener.loginGoogle();
            }

        }
    };

    private View.OnClickListener btnTwitterClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            if (null != mListener) {
                mListener.loginTwitter();
            }
        }
    };

    public interface OnRegisterCallbackListener {
        void loginFacebook();

        void loginGoogle();

        void loginTwitter();
    }

}

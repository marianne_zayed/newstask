package com.example.news.helpers;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.content.res.Configuration;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;

import com.example.news.R;
import com.example.news.models.UserLogin;
import com.google.gson.Gson;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by Marianne on 12-Nov-16.
 */

public class Utility {
    public static int getImageResourceForDrawerItems(int positionId) {
        final int NEWS = 0;
        final int MAP = 1;
        final int CALENDER = 2;
        final int LEADERSHIP = 3;
        final int ARABIC = 4;
        final int LOGOUT = 5;


        switch (positionId) {
            case NEWS: {
                return R.drawable.news_icon;

            }
            case MAP: {
                return R.drawable.map_icon;

            }
            case CALENDER: {
                return R.drawable.events_icon;

            }
            case LEADERSHIP: {
                return R.drawable.leadership_icon;

            }
            case ARABIC: {
                return R.drawable.language;

            }
            case LOGOUT: {
                return R.drawable.events_icon;
            }


        }


        return -1;
    }

    public static int getImageResourceForNewsType(String newsTypeId) {
        switch (newsTypeId) {
            case "84": {
                return R.drawable.article_label;

            }
            case "85": {
                return R.drawable.video_label;

            }


        }
        return -1;
    }

    public static boolean haveNetworkConnection(Context context) {
        boolean haveConnectedWifi = false;
        boolean haveConnectedMobile = false;

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null) {
            if (netInfo.getType() == ConnectivityManager.TYPE_WIFI)
                haveConnectedWifi = true;
            if (netInfo.getType() == ConnectivityManager.TYPE_MOBILE)
                haveConnectedMobile = true;
        }
        return haveConnectedWifi || haveConnectedMobile;
    }


    public static void changeLanguage(String lang, Context context) {

        Locale locale = new Locale(lang);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.setLocale(locale);
        context.getResources().updateConfiguration(config, context.getResources().getDisplayMetrics());
    }

    public static void storedLocale(String lang, Context context) {
        SharedPreferences.Editor editor = context.getSharedPreferences(Constants.LOCALE, MODE_PRIVATE).edit();
        editor.putString(Constants.PREFERENCE_LANG, lang);
        editor.commit();
    }

    public static String formatDate(String Format, String dateStr, Locale newLocale) {
        if (TextUtils.isEmpty(dateStr)) {
            return "";
        }
        SimpleDateFormat fmt = new SimpleDateFormat(Format, Locale.ENGLISH);
        Date date = null;
        try {
            date = fmt.parse(dateStr);
        } catch (ParseException e) {
            e.printStackTrace();
            return dateStr;
        }

        SimpleDateFormat fmtOut = new SimpleDateFormat(Format, newLocale);
        return fmtOut.format(date);
    }

    public static void getHashKey(Context context) {
        try {
            PackageInfo info = context.getPackageManager().getPackageInfo("com.example.news", PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }

    public static void storeLoginData(Context context, UserLogin userLogin) {
//        editor.putString(Constants.USERNAME, username);
//        editor.putString(Constants.EMAIL , email);
//        editor.commit();
        SharedPreferences.Editor editor = context.getSharedPreferences(Constants.LOGIN, MODE_PRIVATE).edit();
        Gson gson = new Gson();
//        UserLogin userLogin = new UserLogin();
//        userLogin.setEmail(email);
//        userLogin.setFullName(username);
//        userLogin.setSocialMedia(socialMedia);
        String jsonUserLogin = gson.toJson(userLogin);
        editor.putString(Constants.USER, jsonUserLogin);
        editor.commit();
    }
}

package com.example.news.helpers;

import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.support.multidex.MultiDexApplication;

import com.example.news.models.UserLogin;
import com.facebook.FacebookSdk;
import com.google.gson.Gson;
import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.TwitterAuthConfig;

import java.util.Locale;

import io.fabric.sdk.android.Fabric;

import static com.example.news.helpers.Constants.TWITTER_KEY;
import static com.example.news.helpers.Constants.TWITTER_SECRET;

/**
 * Created by Marianne.Wazif on 26-Dec-16.
 */

public class GlobalState extends MultiDexApplication {

    private SharedPreferences prefs;

    public String getLocale() {
        prefs = getSharedPreferences(Constants.LOCALE, MODE_PRIVATE);
        return prefs != null ? prefs.getString(Constants.PREFERENCE_LANG, Locale.getDefault().getLanguage()) : Locale.getDefault().getLanguage();
    }

    public void setLocale(String locale) {
        Utility.storedLocale(locale, this);
        Utility.changeLanguage(locale, getBaseContext());
    }

    @Override
    public void onCreate() {
        super.onCreate();
        FacebookSdk.sdkInitialize(this);
        TwitterAuthConfig authConfig = new TwitterAuthConfig(TWITTER_KEY, TWITTER_SECRET);
        Fabric.with(this, new Twitter(authConfig));
        //   prefs = getSharedPreferences(Constants.LOCALE, MODE_PRIVATE);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        Utility.changeLanguage(getLocale(), getBaseContext());
    }

    //    public void setLogin(String username, String email, int socialMedia) {
//        Utility.storeLoginData(this, username, email, socialMedia);
//    }
    public UserLogin getLogin() {
        prefs = getSharedPreferences(Constants.LOGIN, MODE_PRIVATE);
        if (prefs != null && prefs.getString(Constants.USER, null) != null) {
            Gson gson = new Gson();
            String jsonUserLogin = prefs.getString(Constants.USER, "");
            UserLogin userLogin = gson.fromJson(jsonUserLogin, UserLogin.class);

            return userLogin;

        }
        return null;
    }
}

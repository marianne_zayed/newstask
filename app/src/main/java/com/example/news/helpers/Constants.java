package com.example.news.helpers;

/**
 * Created by Marianne.Wazif on 18-Dec-16.
 */

public class Constants {

    public static final String DETAILSTAG = "DetailsRequestTag";
    public static final String NEWSLISTINGFRAGMENT_TAG = "lFTAG";
    public static final String NEWSID = "id";
    public static final String NEWSDETAILSFRAGMENT_TAG = "DFTAG";
    public static final String DETAIL_FRAGMENT_PARAM = "NewsID";
    public static final String LISTING_FRAGMENT_PARAM = "TwoPaneParam";
    public static final String TWO_PANE = "TwoPane";
    public static final String SELECTED_KEY = "SelectedKey";
    public static final String LOCALE_EN = "en";
    public static final String LOCALE_AR = "ar";
    public static final String LOCALE = "Locale";
    public static final String PREFERENCE_LANG = "lang";
    public static final String NEWSLOGIN_TAG = "LGTAG";
    public static final String LOGIN = "Login";
    public static final String USERNAME = "usermame";
    public static final String EMAIL = "email";
    public static final String USER = "user";
    public static final String LOGOUT = "Logout";
    public static final int FACEBOOK = 1;
    public static final int TWITTER = 2;
    public static final int GOOGLE = 3;
    public static final String SOCIALMEDIA = "SocialMedia";
    public static final int RC_SIGN_IN = 1;
    public static final String TWITTER_KEY="eNaUEJca9c6HeJmCmj2O9MEg5";
    public static final String TWITTER_SECRET="CVvJZ1BiGuShM5sPDQpEvHoZbN0PHvkLR6Uapc7851vXGgaSAo";


}

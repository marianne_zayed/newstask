package com.example.news.helpers;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.widget.Toast;

import com.example.news.activities.ActivityListingNews;
import com.example.news.activities.ActivityLogin;
import com.example.news.models.UserLogin;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterApiClient;
import com.twitter.sdk.android.core.TwitterCore;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterAuthClient;
import com.twitter.sdk.android.core.models.User;
import com.twitter.sdk.android.core.services.AccountService;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;

import retrofit2.Call;

import static android.content.Context.MODE_PRIVATE;
import static com.facebook.FacebookSdk.getApplicationContext;

/**
 * Created by Marianne on 02-Jan-17.
 */

public class SocialMediaManager implements GoogleApiClient.OnConnectionFailedListener {
    private GoogleApiClient mGoogleApiClient;
    private static SocialMediaManager mInstance;

    private SocialMediaManager(Context context) {
        mGoogleApiClient = getGoogleApiClient(context);

    }

    public static SocialMediaManager getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new SocialMediaManager(context);
        }
        return mInstance;
    }

    public void facebookRegisterCallBack(Activity context, CallbackManager mFacebookCallbackManager, FacebookCallback<LoginResult> facebookCallback) {
        LoginManager.getInstance().logInWithReadPermissions(
                context,
                Arrays.asList("email", "public_profile"));
        LoginManager.getInstance().registerCallback(mFacebookCallbackManager, facebookCallback);

    }

    public void twitterRegisterCallBack(Activity context, TwitterAuthClient twitterAuthClient, final Callback<User> userCallback) {
        twitterAuthClient.authorize(context, new Callback<TwitterSession>() {
            @Override
            public void success(Result<TwitterSession> result) {

                TwitterSession sessionData = result.data;
                Twitter twitter = Twitter.getInstance();
                TwitterApiClient api = twitter.core.getApiClient(sessionData);
                AccountService service = api.getAccountService();
                getTwitterData(service, userCallback);
            }

            @Override
            public void failure(TwitterException e) {
                userCallback.failure(e);
            }
        });
    }

    public UserLogin setGoogleData(GoogleSignInResult googleSignInResult, Context context) {
        UserLogin userLogin = new UserLogin();
        GoogleSignInAccount acct = googleSignInResult.getSignInAccount();
        if (acct != null) {
            userLogin.setFullName(acct.getDisplayName());
            userLogin.setEmail(acct.getEmail());
            userLogin.setSocialMedia(Constants.GOOGLE);
        }
        return userLogin;
    }

    private void getTwitterData(AccountService service, Callback<User> callback) {
        Call<User> user = service.verifyCredentials(true, true);
        user.enqueue(callback);
    }

    public UserLogin setTwitterData(Result<User> userResult) {
        UserLogin userLogin = new UserLogin();
        userLogin.setFullName(userResult.data.name);
        userLogin.setEmail(userResult.data.email);
        userLogin.setSocialMedia(Constants.TWITTER);
        return userLogin;

    }

    public void getFacebookData(LoginResult loginResult, GraphRequest.GraphJSONObjectCallback GraphJSONObjectCallback) {
        GraphRequest request = GraphRequest.newMeRequest(
                loginResult.getAccessToken(),
                GraphJSONObjectCallback);
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,email,first_name,last_name");
        request.setParameters(parameters);
        request.executeAsync();
    }

    public UserLogin setFacebookData(GraphResponse response) {
        UserLogin userLogin = new UserLogin();
        try {
            userLogin.setEmail(response.getJSONObject().getString("email"));
            String firstName = response.getJSONObject().getString("first_name");
            String lastName = response.getJSONObject().getString("last_name");
            String fullName = firstName + " " + lastName;
            userLogin.setFullName(fullName);
            userLogin.setSocialMedia(Constants.FACEBOOK);
        } catch (JSONException e) {
            Log.e("error", "onConnectionFailed:" + e);
        }
        return userLogin;
    }

    public void disconnectFromFacebook() {

        if (AccessToken.getCurrentAccessToken() == null) {
            return; // already logged out
        }
        new GraphRequest(AccessToken.getCurrentAccessToken(), "/me/permissions/", null, HttpMethod.DELETE, new GraphRequest
                .Callback() {
            @Override
            public void onCompleted(GraphResponse graphResponse) {
                LoginManager.getInstance().logOut();
            }
        }).executeAsync();
    }

    public GoogleApiClient getGoogleApiClient(Context context) {

        if (mGoogleApiClient == null) {
            GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                    .requestEmail()
                    .build();
            mGoogleApiClient = new GoogleApiClient.Builder(context)
                    .enableAutoManage((FragmentActivity) context /* FragmentActivity */, this /* OnConnectionFailedListener */)
                    .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                    .build();
        }
        return mGoogleApiClient;

    }

    public void logoutGoogle() {
        mGoogleApiClient.connect();
        mGoogleApiClient.registerConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
            @Override
            public void onConnected(@Nullable Bundle bundle) {

                //FirebaseAuth.getInstance().signOut();
                if (mGoogleApiClient.isConnected()) {
                    Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(new ResultCallback<Status>() {
                        @Override
                        public void onResult(@NonNull Status status) {
                            if (status.isSuccess()) {
                                Log.d("success", "User Logged out");
                            }
                        }
                    });
                }
            }

            @Override
            public void onConnectionSuspended(int i) {
                Log.d("suspended", "Google API Client Connection Suspended");
            }
        });
    }

    public void logoutTwitter() {
        TwitterSession twitterSession = TwitterCore.getInstance().getSessionManager().getActiveSession();
        if (twitterSession != null) {
            ClearCookies(getApplicationContext());
            Twitter.getSessionManager().clearActiveSession();
            Twitter.logOut();
        }

    }

    private static void ClearCookies(Context context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
            CookieManager.getInstance().removeAllCookies(null);
            CookieManager.getInstance().flush();
        } else {
            CookieSyncManager cookieSyncMngr = CookieSyncManager.createInstance(context);
            cookieSyncMngr.startSync();
            CookieManager cookieManager = CookieManager.getInstance();
            cookieManager.removeAllCookie();
            cookieManager.removeSessionCookie();
            cookieSyncMngr.stopSync();
            cookieSyncMngr.sync();
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.d("error", "onConnectionFailed:" + connectionResult);
    }


}

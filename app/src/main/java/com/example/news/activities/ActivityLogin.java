package com.example.news.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.example.news.R;
import com.example.news.fragments.FragmentLogin;
import com.example.news.helpers.Constants;
import com.example.news.helpers.SocialMediaManager;
import com.example.news.helpers.Utility;
import com.example.news.models.UserLogin;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.identity.TwitterAuthClient;
import com.twitter.sdk.android.core.models.User;

import org.json.JSONObject;

import static com.example.news.helpers.Constants.RC_SIGN_IN;

public class ActivityLogin extends AppCompatActivity implements FragmentLogin.OnRegisterCallbackListener {
    private CallbackManager mFacebookCallbackManager;
    private TwitterAuthClient mTwitterAuthClient;

    public static void startActivity(Context context) {
        Intent i = new Intent(context, ActivityLogin.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(i);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        mFacebookCallbackManager = CallbackManager.Factory.create();
        mTwitterAuthClient = new TwitterAuthClient();
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction().add(R.id.frmFragmentLoginContainer, FragmentLogin.newInstance(), Constants.NEWSLOGIN_TAG).commit();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mFacebookCallbackManager.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            if (result.isSuccess()) {
                UserLogin userLogin = SocialMediaManager.getInstance(this).setGoogleData(result, this);
                Utility.storeLoginData(this, userLogin);
                ActivityListingNews.startActivity(getApplicationContext());
                finish();
            } else {
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.login_failed), Toast.LENGTH_SHORT).show();
            }
        }
        mTwitterAuthClient.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void loginFacebook() {
        SocialMediaManager.getInstance(this).facebookRegisterCallBack(this, mFacebookCallbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        SocialMediaManager.getInstance(getApplicationContext()).getFacebookData(loginResult, new GraphRequest.GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(JSONObject object, GraphResponse response) {

                                Log.i("Response", response.toString());
                                UserLogin userLogin = SocialMediaManager.getInstance(getApplicationContext()).setFacebookData(response);
                                Utility.storeLoginData(getApplicationContext(), userLogin);
                                ActivityListingNews.startActivity(getApplicationContext());
                                finish();
                            }
                        });
                    }

                    @Override
                    public void onCancel() {
                        Log.e("error", "CANCEL");
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.login_failed), Toast.LENGTH_SHORT).show();
                        exception.printStackTrace();
                    }
                });
    }

    @Override
    public void loginGoogle() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(SocialMediaManager.getInstance(this).getGoogleApiClient(this));
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }
    @Override
    public void loginTwitter() {
        SocialMediaManager.getInstance(this).twitterRegisterCallBack(this, mTwitterAuthClient, new Callback<User>() {
            @Override
            public void success(Result<User> userResult) {
                UserLogin userLogin = SocialMediaManager.getInstance(getApplicationContext()).setTwitterData(userResult);
                Utility.storeLoginData(getApplicationContext(), userLogin);
                ActivityListingNews.startActivity(getApplicationContext());
                finish();
            }

            @Override
            public void failure(TwitterException exc) {
                Log.d("TwitterKit", "Verify Credentials Failure", exc);
                Toast.makeText(getApplicationContext(), "Login failed", Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}

package com.example.news.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Window;
import android.view.WindowManager;

import com.example.news.helpers.Constants;
import com.example.news.R;
import com.example.news.fragments.FragmentNewsDetails;

public class ActivityNewsDetails extends AppCompatActivity {
    private boolean mTwoPane;
    private String mNewsID;

    public static void startActivity(Context context, String id, boolean twoPane) {
        Intent i = new Intent(context, ActivityNewsDetails.class);
        i.putExtra(Constants.NEWSID, id);
        i.putExtra(Constants.TWO_PANE, twoPane);
        context.startActivity(i);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news_details);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(this.getResources().getString(R.string.title_activity_news_details));
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        mTwoPane = getIntent().getExtras().getBoolean(Constants.TWO_PANE);
        mNewsID = getIntent().getExtras().getString(Constants.NEWSID);

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction().add(R.id.frmFragmentNewsDetailsContainer, FragmentNewsDetails.newInstance(mTwoPane, mNewsID), Constants.NEWSDETAILSFRAGMENT_TAG).commit();
        }
    }
}

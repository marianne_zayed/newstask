package com.example.news.activities;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;

import com.example.news.R;
import com.example.news.helpers.GlobalState;
import com.example.news.models.UserLogin;

public class SplashActivity extends Activity {
    private int SPLASH_TIME_OUT = 3000;
    private UserLogin mUserLogin = new UserLogin();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        GlobalState mApplication = (GlobalState) getApplication();
        mUserLogin = mApplication.getLogin();
        new Handler().postDelayed(new Runnable() {

            /*
             * Showing splash screen with a timer. This will be useful when you
             * want to show case your app logo / company
             */

            @Override
            public void run() {
                // This method will be executed once the timer is over
                // Start your app main activity
                if (mUserLogin != null) {
                    ActivityListingNews.startActivity(getApplicationContext());
                } else {
                    ActivityLogin.startActivity(getApplicationContext());
                }// close this activity
                finish();
            }
        }, SPLASH_TIME_OUT);
    }

}



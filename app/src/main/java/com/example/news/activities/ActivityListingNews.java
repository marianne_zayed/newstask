package com.example.news.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import com.example.news.fragments.FragmentNewsDetails;
import com.example.news.helpers.Constants;
import com.example.news.adapters.DrawerItemsAdapter;
import com.example.news.fragments.FragmentListingNews;
import com.example.news.R;
import com.example.news.helpers.GlobalState;
import com.example.news.helpers.SocialMediaManager;
import com.example.news.helpers.Utility;
import com.example.news.models.News;
import com.example.news.models.UserLogin;

import java.util.ArrayList;
import java.util.Arrays;

public class ActivityListingNews extends AppCompatActivity
        implements FragmentListingNews.OnListFragmentInteractionListener, DrawerItemsAdapter.DrawerItemClickListener {

    private DrawerLayout mdrawer;
    private RecyclerView mRecLeftDrawer;
    private ArrayList<String> mDrawerItemsTitles = new ArrayList<String>();
    private DrawerItemsAdapter mDrawerItemsAdapter;
    private SearchView msearch;
    private boolean mTwoPane;
    private String mCurrentLang;
    private GlobalState mApplication;
    private ArrayList<UserLogin> mUserLoginArrayList;
    private UserLogin mUserLogin = new UserLogin();

    public static void startActivity(Context context) {
        Intent i = new Intent(context, ActivityListingNews.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(i);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mApplication = (GlobalState) getApplication();
        mCurrentLang = mApplication.getLocale();
        Utility.changeLanguage(mCurrentLang, this);
        mUserLogin = mApplication.getLogin();
        setContentView(R.layout.activity_listing_news);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(this.getResources().getString(R.string.activity_listing_news_name));
        setSupportActionBar(toolbar);
        mTwoPane = (findViewById(R.id.frmFragmentNewsDetailsContainer) != null);
        if (savedInstanceState == null) {

            getSupportFragmentManager().beginTransaction().add(R.id.frmFragmentNewsListingContainer, FragmentListingNews.newInstance(mTwoPane), Constants.NEWSLISTINGFRAGMENT_TAG).commit();
        }
        mdrawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        mRecLeftDrawer = (RecyclerView) findViewById(R.id.recLeftDrawer);
        mDrawerItemsTitles = new ArrayList<>(Arrays.asList(getResources().getStringArray(R.array.drawer_items_Titles)));
        mRecLeftDrawer.setLayoutManager(new LinearLayoutManager(this));

        mDrawerItemsAdapter = new DrawerItemsAdapter(mDrawerItemsTitles, mUserLogin, this);

        mRecLeftDrawer.setAdapter(mDrawerItemsAdapter);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, mdrawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        mdrawer.addDrawerListener(toggle);
        toggle.syncState();
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        mUserLogin = mApplication.getLogin();
        if (mUserLogin == null) {
            ActivityLogin.startActivity(this);
            finish();
        }

    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem searchViewMenuItem = menu.findItem(R.id.action_search);
        SearchView mSearchView = (SearchView) MenuItemCompat.getActionView(searchViewMenuItem);
        int searchImgId = android.support.v7.appcompat.R.id.search_button; // I used the explicit layout ID of searchview's ImageView
        ImageView v = (ImageView) mSearchView.findViewById(searchImgId);
        v.setImageResource(R.drawable.filter);
        return super.onPrepareOptionsMenu(menu);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        msearch = (SearchView) menu.findItem(R.id.action_search).getActionView();
        msearch.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                return false;
            }
        });
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onListFragmentInteraction(News news) {

        if (!mTwoPane) {
            ActivityNewsDetails.startActivity(this, news.getNid(), mTwoPane);
        } else {

            getSupportFragmentManager().beginTransaction().replace(R.id.frmFragmentNewsDetailsContainer, FragmentNewsDetails.newInstance(mTwoPane, news.getNid()), Constants.NEWSDETAILSFRAGMENT_TAG).commit();

        }


    }

    @Override
    public void onClick(View view, int position) {
        switch (position) {
            case 2:{
                ActivityGoogleMaps.startActivity(this);
                break;
            }
            case 5: {
                toggleAppLanguage(mCurrentLang);
                break;
            }
            case 6: {
                if (mUserLogin.getSocialMedia() == Constants.FACEBOOK) {
                    SocialMediaManager.getInstance(this).disconnectFromFacebook();
                } else if (mUserLogin.getSocialMedia() == Constants.GOOGLE) {
                    SocialMediaManager.getInstance(this).logoutGoogle();
                } else if (mUserLogin.getSocialMedia() == Constants.TWITTER) {
                    SocialMediaManager.getInstance(this).logoutTwitter();

                }
                SharedPreferences preferences = getSharedPreferences(Constants.LOGIN, MODE_PRIVATE);
                preferences.edit().remove(Constants.USER).apply();
                ActivityLogin.startActivity(this);
                finish();
                break;

            }

        }

    }

    private void toggleAppLanguage(String lang) {

        if (lang.equals(Constants.LOCALE_EN)) {
            mApplication.setLocale(Constants.LOCALE_AR);
        } else {
            mApplication.setLocale(Constants.LOCALE_EN);
        }
        finish();
        startActivity(getIntent());
    }


    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        if (getResources().getBoolean(R.bool.tablet_orientation)) {
            finish();
            startActivity(getIntent());
        }
    }

}
